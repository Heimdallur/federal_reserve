## Federal_reserve ##

scraper for 'federalreserve.gov'

## Getting Started ##



```
#!bash

git clone https://bitbucket.org/heimdallur/federal_reserve
cd federal_reserve
virtualenv federal_reserve_venv
source federal_reserve_venv/bin/activate
```


```
#!bash

pip install -r requirements.txt
cd federal_reserve
```


```
#!bash

scrapy crawl federalSpider
```


each "speech" will be added as a separate .pdf file in "articles" directory
