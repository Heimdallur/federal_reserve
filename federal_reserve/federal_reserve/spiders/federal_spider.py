#!/usr/bin/env python
# coding: utf-8

import scrapy
import codecs
from pypdflite import PDFLite
from federal_reserve.items import FederalReserveItem


class FederalSpider(scrapy.Spider):
    name = "federalSpider"
    allowed_domains = ["www.federalreserve.gov"]
    start_urls = [
        "https://www.federalreserve.gov/newsevents/speech/2015speech.htm"
    ]

    def parse(self, response):
        for href in response.css('ul[id*="speechIndex"] > li'):
            item = FederalReserveItem()
            item['date_article'] = href.xpath('text()').extract()[0].replace('      \r\n      ', '')
            item['title'] = href.xpath('div/a/text()').extract()[0]
            item['link'] = href.xpath('div/a/@href').extract()[0]
            item['speaker'] = href.xpath('div[@class="speaker"]/text()').extract()[0]
            item['location'] = href.xpath('div[@class="location"]/text()').extract()
            url = response.urljoin(item['link'])

            request = scrapy.Request(url, callback=self.parse_article, encoding='iso-8859-1')
            request.meta['item'] = item

            yield request

    def parse_article(self, response):
        item = response.meta['item']
        text = ''
        for paragraph in response.css('div[id*="leftText"] > p'):
            text += ' '.join(paragraph.xpath('text()').extract())

        item['article'] = text
        self.write_to_pdf(item)

        return item

    def write_to_pdf(self, item):
        writer = PDFLite("articles/%s %s.pdf" % (item['title'], item['date_article'].replace(',', '')), layout='a4')
        document = writer.get_document()
        document.set_font(family='ubuntu-b')
        document.add_text(item['speaker'].encode('iso-8859-1')+'\n')
        document.add_text(item['date_article']+'\n')
        document.add_text(item['title'])
        document.add_text('\n')
        document.add_text(item['article'].replace(u'\u2013', '-').encode('iso-8859-1', 'ignore')+'\n')
        writer.close()


        # print item['article'].encode('utf-8')
        # f = codecs.open("articles/%s.txt" % item['title'], encoding='utf8', mode='w+')
        # f.write(item['article'])
        # f.close()


        # f = open("articles/%s.txt" % item['title'], 'w+')
        # f.write(item['speaker']+'\n')
        # f.write(item['date_article']+'\n')
        # f.write(item['article']+'\n')
        # f.close()

